#!/bin/bash

v1=$1
v2=$2

if [ $v1 -lt $v2 -a -e "/bin/bash" ]
then
	echo $v1"<"$v2
else
	echo $v1">"$v2
fi
