#!/bin/bash

chaine1=$1
chaine2=$2

if [ $chaine1 == $chaine2 ]
then
	echo $chaine1"=="$chaine2
else
	echo $chaine1"!="$chaine2
fi
