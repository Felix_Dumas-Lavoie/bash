#!/bin/bash


#Par défaut la fonction dans son bloc va avoir accès aux mêmes paramètres 
#que 
f()
{
	echo "Le premier argument de la fonction f est $1"
	echo "Le second argument de la fonction f est $2"
}

echo "Le premier argument du script est: $1"
echo "Le 2e argument du script est: $2"

f $2 $1

