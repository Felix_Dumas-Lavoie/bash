#!/bin/bash

if [ -e $1  ]
then
	echo "La ressource "`basename $1`" existe."
else
	echo "La ressource "`basename $1`" n'existe pas."
fi

