#!/bin/bash

if [ -d $1 ];
then
	echo "La ressource $1 existe."
fi

if [ ! -d $1 ];
then
	echo "La ressource $1 n'existe pas."
fi
